import React from 'react';
import Product from './components/Product'
import ProductData from './components/schoolProducts'
 function App(){
  const ProductComponent = ProductData.map(item => <Product key={item.id}  product={item}/>)
   return(
     <div>
       {ProductComponent}
     </div>
   )

}

export default App
