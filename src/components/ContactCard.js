import React from 'react'
function ContactCard(props){
    console.log(props)
    return(
    <div className="card">
    <img src={props.imgUrl} alt={props.alt} style={{display:!props.imgUrl && "none"}} />
    <h1>{props.name}</h1>
    <p className="title">Phone: {props.phone}</p>
    <p>Email : {props.email}</p>
    </div>
    )
}
export default ContactCard